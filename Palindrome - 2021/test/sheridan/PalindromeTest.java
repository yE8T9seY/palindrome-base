package sheridan;

/// ANUJ CHOUDHARY

// 991539753
 
import static org.junit.Assert.*;
 
import org.junit.Test;
 
public class PalindromeTest {
 
    @Test
    public void testIsPalindromeRegular( ) {
        boolean test=Palindrome.isPalindrome("racecar");
        assertTrue("Value provided failed palindrome validation",test);

    }
 
    @Test
    public void testIsPalindromeNegative( ) {
        boolean test=Palindrome.isPalindrome("monkeyKela");
        assertFalse( "Test not created.",test);
    }
    @Test
    public void testIsPalindromeBoundaryIn( ) {
        boolean test=Palindrome.isPalindrome("aiboh phobia");
        assertTrue( "Failed",test );
    }
    @Test
    public void testIsPalindromeBoundaryOut( ) {
        boolean test=Palindrome.isPalindrome("dad mom");
        assertFalse( "Fails",test );
    }    

}